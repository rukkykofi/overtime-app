# Overtime App

## Key requirement
company needs documentation that salaried employees did or did not get overtime each week:

## Models
- Post -> data:date rationale:text
- User -> Devise
- AdminUser -> STI

## Features
- Approval Workflow
- SMS Sending -> link to approval or overtime input
- Administrate admin dashboard
- Email summary to managers for approval
- Needs to be documented if employee did or did not log overtime

## UI:
- Bootstrap -> formatting

## Refactor TODOs:
- Refactor user association integration test in post_spec